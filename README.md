# FMIS Daisy Wrapper

This is a Daisy Wrapper Service.

## Important note for windows users in development environment:
- To be able to run the server correct, 
	change line endings to the linux standard 'LF'
	* To do this open the dockerRun.sh file ->
		click Edit -> Advanced -> Set End of Line Sequence -> LF

## How to Windows / linux / mac:
- (sudo) docker build -t fmis-daisywrapper .
- (sudo) docker run --rm -it -p 80:80 -e ASPNETCORE_URLS="http://+:80" fmis-daisywrapper
 
## How to Cloud Run
The service cannot be used on Cloud Run becuase of ristricted storage permissions.
There is file operations done on local directories, and the daisy instance is runned by this.
You can run this on a Google Cloud VM

## DaisyWrapper in a Compute Engine
You do not need gcloud to make a compute engine.

### On google cloud platform
- Go to Compute Engine Tab
- Create new instance
	- Select 
		Allow HTTP traffic
		Allow HTTPS traffic
- Note the external ip to your vm instance, you need this to connect to your vm, or reach the db

- Go to VPC network
- Go to Firewall
- Check if the port you need to be open is open or add new firewall for the port


### Connect via ssh to the vm (or via google cloud platform's interface)
- Install docker (http://andrewcmaxwell.com/2016/07/how-to-setup-and-install-docker-on-google-compute-engine)
- Install docker compose (https://docs.docker.com/compose/install/)

Open up the connection to the outside world
- Run 'sysctl net.ipv4.conf.all.forwarding=1'
- Run 'sudo iptables -P FORWARD ACCEPT'

Connect to the server via either WinSCP, or Putty, with a ssh key
 - See https://cloud.google.com/compute/docs/instances/transfer-files

- Create a file on the server with ex. touch and nano commands "docker-compose.yml"
	- You can get inspiration for what is needed in docker-compose.yml

- run 'docker-compose up -d' to start the services
- Now you can reach the daisy wrapper service on the external ip mentioned earlier.

To destroy the running container again, use 'docker-compose down' 