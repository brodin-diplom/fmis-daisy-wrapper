# THIS A DOCKERFILE FOR RUNNING ON GOOGLE CLOUD
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /app

# Install production dependencies.
# Copy csproj and restore as distinct layers.
COPY DaisyWrapper/*.csproj ./
RUN dotnet restore

# Copy local code to the container image.
COPY ./DaisyWrapper/ ./
WORKDIR /app

# Build a release artifact.
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS runtime
WORKDIR /app
EXPOSE 80
ENV ASPNETCORE_ENVIRONMENT=Development
#ENV ASPNETCORE_URLS=https://+:6001;http://+:6000
COPY DaisyWrapper/ /app/
COPY --from=build /app/out ./
# Run the service on container startup.
ENTRYPOINT ["dotnet", "DaisyWrapper.dll"]


