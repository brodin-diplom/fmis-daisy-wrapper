﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace DaisyWrapper.Helper
{
    public class DaisyExcecution
    {

        // Run multiple files
        public static Task RunManagementFiles(List<string> managementFileNames, string inputDir, string outputDir)
        {
            // All other than managementFiles
            List<FileInfo> supportFiles = new();
            List<FileInfo> managementFiles = new();
            List<DirectoryInfo> managementFolders = new();

            var inputDirInfo = new DirectoryInfo(inputDir);

            // Gather all fileinfo's
            foreach (var fileInfo in inputDirInfo.GetFiles())
            {
                if (managementFileNames.Contains(fileInfo.Name))
                    managementFiles.Add(fileInfo);
                else
                    supportFiles.Add(fileInfo);
            }

            // Copy files to each management file folder
            foreach (var fileInfo in managementFiles)
            {
                string managementName = fileInfo.Name.Split(".")[0];
                // Define new directory
                var targetDirPath = Path.Combine(inputDirInfo.FullName, managementName);

                // Copy all files to new folder
                var targetDirInfo = CopyAllFilesToNewFolder(supportFiles, fileInfo, targetDirPath);
                
                // Add the new folder to the managementfolders
                managementFolders.Add(targetDirInfo);
            }

            // Cleanup after copying
            managementFiles.ForEach(file => { file.Delete(); });
            supportFiles.ForEach(file => { file.Delete(); });

            var daisyTasks = new List<Task>();

            foreach (var folderInfo in managementFolders)
            {
                var daisyManagementFile = folderInfo.Name + ".dai";
                var daisyTask = Task.Run(() => RunOneFileAsync(managementFile: daisyManagementFile, inputDir: folderInfo.FullName, outputDir: outputDir));
                daisyTasks.Add(daisyTask);
            }

            return Task.WhenAll(daisyTasks);
        }


        private static async Task<bool> RunOneFileAsync(string managementFile, string inputDir, string outputDir)
        {
            Console.WriteLine($"RunOneFile({managementFile}, {inputDir}, {outputDir})");
            // There needs to be a outputfolder in the input folder to compile with daisy.
            string inputOutputDir = Path.Combine(inputDir, "Output");
            if (!Directory.Exists(inputOutputDir))
            {
                Directory.CreateDirectory(inputOutputDir);
            }

            // There needs to be a folder to move outputfiles to
            string managementName = managementFile.Split(".")[0];
            string managementOutputDir = Path.Combine(outputDir, managementName);
            if (!Directory.Exists(managementOutputDir))
            {
                Directory.CreateDirectory(managementOutputDir);
            }

            // Start daisy process
            var process = new Process
            {
                StartInfo =
                {
                    FileName = "/opt/daisy/bin/daisy",
                    Arguments = $"-d {inputDir} {managementFile}"
                }
            };

            process.Start();
            await process.WaitForExitAsync();
            process.Dispose();

            // Open output folder
            DirectoryInfo inDir = new(inputOutputDir);

            // Go over all files and move them to the new directory
            foreach (var fileInfo in inDir.GetFiles())
            {
                File.Move(sourceFileName: fileInfo.FullName, destFileName: Path.Combine(managementOutputDir + "/" + fileInfo.Name));
            }

            // Move the log file aswell
            string logFileName = "daisy.log";
            string fullPathToLogFile = $"{inputDir}/{logFileName}";
            
            if (File.Exists(path: fullPathToLogFile))
            {
                File.Move(sourceFileName: fullPathToLogFile, destFileName: $"{managementOutputDir}/{logFileName}");
            }
            return true;
        }

        private static DirectoryInfo CopyAllFilesToNewFolder(List<FileInfo> filesToCopy, FileInfo managementFile, string newOutputDir)
        {
            if (!Directory.Exists(newOutputDir))
            {
                Directory.CreateDirectory(newOutputDir);
            }

            // Copy support files
            foreach (var fileInfo in filesToCopy)
            {
                fileInfo.CopyTo(Path.Combine(newOutputDir, fileInfo.Name));
            }

            // Copy management file
            managementFile.CopyTo(Path.Combine(newOutputDir, managementFile.Name));

            return new DirectoryInfo(newOutputDir);
        }
    }
}
