﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.IO.Compression;
using DaisyWrapper.Helper;
using System;

namespace Daisy_Wrapper.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DaisyWrapperController : ControllerBase
    {
        const string defaultPathInput = "daisyInput";
        const string defaultPathOutput = "daisyOutput";
        const string defaultZoneToRun = "Zone_1_1.dai";
        const string testFolderName= "test";
        const string projectFolder = "/app";
        const string zipDir = "resultZipFolders";

        /// <summary>
        /// Tests a daisy run
        /// </summary>
        /// <returns>A newly created Zipfile</returns>
        /// <response code="200">Returns the newly created zip-file with the results</response>
        /// <response code="500">If any error occured</response>
        [HttpGet]
        [Route("test")]
        public async Task<IActionResult> TestDaisy()
        {
            try
            {
                // Define directories
                string randomName = Path.GetRandomFileName().Split(".")[0];
                string execInputDir = Path.Combine(projectFolder, defaultPathInput, testFolderName);
                string execOutputDir = Path.Combine(projectFolder, defaultPathOutput, testFolderName + randomName);

                // Execute Daisy on the folder
                await DaisyExcecution.RunManagementFiles(inputDir: execInputDir, outputDir: execOutputDir, managementFileNames: new List<string> { defaultZoneToRun });

                // Define folders / names for zip output
                string zipName = $"{testFolderName}-{randomName}.zip";
                string outputDirZip = Path.Combine(projectFolder, zipDir);
                string outputFullDirZip = Path.Combine(outputDirZip, zipName);

                // Create zip from the directory from the output from Daisy
                ZipFile.CreateFromDirectory(execOutputDir, outputFullDirZip);

                // Open up a stream for the zip to download.
                var zipMemory = new MemoryStream();
                using (var stream = new FileStream(outputFullDirZip, FileMode.Open))
                {
                    await stream.CopyToAsync(zipMemory);
                }

                zipMemory.Position = 0;

                // Delete the outputDir
                DeleteFilesAndDirectory(execOutputDir);

                // Return the zipFile
                return File(zipMemory, contentType: "application/octet-stream", fileDownloadName: zipName);
            
            }
            catch
            {
                return StatusCode(500, $"Internal server error, please contact support");
            }
        }

        /// <summary>
        /// Upload files for simulation
        /// </summary>
        /// <param name="files_to_run"></param>
        /// <param name="files"></param>
        /// <remarks>
        /// The management to run needs to either conform to the standard 'Zone_' (ex. Zone_1_1.dai) 
        /// or needs to be parsed through QueryParams under the key **"files_to_run"**.
        /// </remarks>
        /// <returns>A newly created Zipfile</returns>
        /// <response code="200">Returns the newly created item</response>
        /// <response code="400">If no files were uploaded</response>
        /// <response code="412">If no management files were uploaded, or the **files_to_run** did not match with any uploaded files</response>
        /// <response code="500">If any other error occured</response>
        [HttpPost]
        [Route("simulate")]
        public async Task<IActionResult> OnPostUploadAsync([FromQuery] List<string> files_to_run, [FromForm] ICollection<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);
            List<string> simulationFiles = new();
            bool defaultManagementRun = true;

            if (files_to_run.Count != 0)
                defaultManagementRun = false;
                

            // Define input and output folders from request
            string defaultManagementPrefix = "Zone_";
            string uploadId = Path.GetRandomFileName().Split(".")[0];
            string inputDir = Path.Combine(projectFolder, defaultPathInput, uploadId);
            string outputDir = Path.Combine(projectFolder, defaultPathOutput, uploadId);
            string zipName = $"download-{uploadId}.zip";
            string outputDirZip = Path.Combine(projectFolder, zipDir);
            string outputFullDirZip = Path.Combine(outputDirZip, zipName);

            // Create input dir if it does not exist
            if (!Directory.Exists(inputDir))
            {
                Directory.CreateDirectory(inputDir);
            }

            // Create output dir if it does not exist
            if (!Directory.Exists(outputDir))
            {
                Directory.CreateDirectory(outputDir);
            }

            if (files.Count == 0)
            {
                return StatusCode(400, "No files were uploaded");
            }

            // Save temp files as the correct filenames
            foreach (var formFile in files)
            {
                // If the file is not empty
                if (formFile.Length > 0)
                {
                    var originalFileName = formFile.FileName ?? Path.GetRandomFileName();

                    var newFilePath = Path.Combine(inputDir, originalFileName);

                    // Check if file is a management file
                    if (defaultManagementRun)
                    {
                        if (originalFileName.StartsWith(defaultManagementPrefix))
                        {
                            simulationFiles.Add(originalFileName);
                        }
                    } else
                    {
                        if (files_to_run.Contains(originalFileName))
                        {
                            simulationFiles.Add(originalFileName);
                        }
                    }

                    // Save the file
                    using var stream = System.IO.File.Create(newFilePath);
                    await formFile.CopyToAsync(stream);
                }
            }

            if (simulationFiles.Count == 0)
            {
                // Delete the inputDir
                DeleteFilesAndDirectory(inputDir);

                if (defaultManagementRun)
                {
                    return StatusCode(412, "No management files with the prefix 'Zone_' or were uploaded, upload correct or use the queryparams 'files_to_run' to specify other files");
                } else
                {
                    return StatusCode(412, $"No management files matched with the 'files_to_run' list: [{String.Join(", ", files_to_run)}]");
                }
                
            }

            // Get daisyrwapper to upload what you have posted, and then save it in outputDir
            await DaisyExcecution.RunManagementFiles(inputDir: inputDir, outputDir: outputDir, managementFileNames: simulationFiles);

            // If the zip output folder does not exist create it.
            if (!Directory.Exists(outputDirZip))
            {
                Directory.CreateDirectory(outputDirZip);
            }

            // Create zip from the directory from the output from Daisy
            ZipFile.CreateFromDirectory(outputDir, outputFullDirZip);

            try
            {
                // Open up a stream for the zip to download.
                var zipMemory = new MemoryStream();
                using (var stream = new FileStream(outputFullDirZip, FileMode.Open))
                {
                    await stream.CopyToAsync(zipMemory);
                }

                zipMemory.Position = 0;

            // Delete the inputDir
            DeleteFilesAndDirectory(inputDir);

            // Delete the outputDir
            DeleteFilesAndDirectory(outputDir);

            // Delete old zipfiles
            DeleteOldZipFiles();

            return File(zipMemory, contentType: "application/octet-stream", fileDownloadName: zipName);
                

            }
            catch
            {
                // Delete the inputDir
                DeleteFilesAndDirectory(inputDir);

                // Delete the outputDir
                DeleteFilesAndDirectory(outputDir);

                // Delete old zipfiles
                DeleteOldZipFiles();

                return StatusCode(500, $"Internal server error, please contact support");
            }

        }

        /// <summary>
        /// Cleanup the directories by deleting the files and dirs from input and output
        /// </summary>
        /// <param name="directory"></param>
        private void DeleteFilesAndDirectory(string directory)
        {
            var dirInfo = new DirectoryInfo(directory);

            // Check for subdirectories, and recursively delete subfolders
            foreach (var subDirInfo in dirInfo.GetDirectories())
            {
                DeleteFilesAndDirectory(subDirInfo.FullName);
            }

            // Check for files, and delete files all
            foreach (var fileInfo in dirInfo.GetFiles())
            {
                fileInfo.Delete();
            }

            // Delete the folder, now that the folder should be empty.
            dirInfo.Delete();
        }

        /// <summary>
        /// Go through the zipFiles created, and delete those which are older than 1 day from creation
        /// </summary>
        private void DeleteOldZipFiles()
        {
            var dirInfo = new DirectoryInfo(Path.Combine(projectFolder, zipDir));

            foreach (var fileInfo in dirInfo.GetFiles())
            {
                // If the file was created more than a day ago
                if (fileInfo.CreationTime.AddDays(1.0) <= DateTime.Now) {
                    fileInfo.Delete();
                }
            }
        }
    }
}