﻿#!/bin/bash
apt-get update
apt-get install libcxsparse3 -y
apt-get install curl -y
apt-get install apt-utils -y
curl https://daisy.ku.dk/download/daisy_6.09_amd64.deb > daisy.deb
dpkg -i ./daisy.deb
rm -f daisy.deb
chmod -R 755 /app/daisyInput
chmod -R 755 /app/daisyOutput
/opt/daisy/bin/daisy -v
rm -f daisy.log
exit